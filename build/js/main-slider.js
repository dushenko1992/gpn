var swiperIntro = new Swiper('.js-intro-slider', {
  slidesPerView: 1,
  spaceBetween: 0,
  slidesPerGroup: 1,
  loop: true,
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});








var swiperPartner = new Swiper('.js_partner-slider', {
  slidesPerView: 9,
  spaceBetween: 0,
  slidesPerGroup: 1,
  loop: true,
  freeMode: true,
  loop: true,
  autoplay: {
    delay: 0,
  },
  speed: 7000,
  allowTouchMove: true,
  paginationClickable: true,
  autoplayDisableOnInteraction: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

var swiperGame = new Swiper('.js_game-slider', {
  slidesPerView: 5,
  spaceBetween: 12,
  slidesPerGroup: 1,
  loop: true,
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

var swiperPresent = new Swiper('.js_present-slider', {
  slidesPerView: 5,
  spaceBetween: 12,
  slidesPerGroup: 1,
  loop: true,
  loopFillGroupWithBlank: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
