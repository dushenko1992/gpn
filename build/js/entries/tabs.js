$(document).ready(function () {
  //tabs
  $(function () {
    $(".js-tabs-item").on("click", "li:not(.active)", function () {
      $(this)
        .addClass("active")
        .siblings()
        .removeClass("active")
        .closest("div.js-tabs")
        .find("div.js-tabs-content")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
    });
  });
});
