//=require ../blocks/**/*.js
$(document).ready(function () {
  const anchors = document.querySelectorAll("a.scroll-link");

  for (let anchor of anchors) {
    anchor.addEventListener("click", function (e) {
      e.preventDefault();
      const blockID = anchor.getAttribute("href").substr(1);
      $(".scroll-link").removeClass("active");
      $(this).addClass("active");
      $(".logo__second").addClass("active");
      $(".logo__first").addClass("active");
      $(".header").addClass("active");
      $(".js-nav").removeClass("active");
      $(".js-burger").removeClass("active");
      $("body").removeClass("overflow");
      document.getElementById(blockID).scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
    });
  }

  // $('select').niceSelect();

});
