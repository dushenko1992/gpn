$(document).ready(function () {
  $(".js-toggle-refuel").click(function () {
    // $(".js-toggle-lk").removeClass("active");
    $(this).toggleClass("active");
    $(".js-refuel-lk").toggleClass("active");
    $(".js-header-contacts").toggleClass("active");
    $(".js-mask").toggleClass("active");
    $("body").toggleClass("overflow");
  });

  $(".js-toggle-loyalty").click(function () {
    // $(".js-toggle-lk").removeClass("active");
    $(this).toggleClass("active");
    $(".js-loyalty-lk").toggleClass("active");
    $(".js-header-contacts").toggleClass("active");
    $(".js-mask").toggleClass("active");
    $("body").toggleClass("overflow");
  });

  $(".js-toggle-road").click(function () {
    // $(".js-toggle-lk").removeClass("active");
    $(this).toggleClass("active");
    $(".js-road-lk").toggleClass("active");
    $(".js-header-contacts").toggleClass("active");
    $(".js-mask").toggleClass("active");
    $("body").toggleClass("overflow");
  });
});
